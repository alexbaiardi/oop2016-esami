package a01a.e1;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class SequenceBuilderImpl<X> implements SequenceBuilder<X> {

	private List<X> sequence=new LinkedList<>();
	private boolean built=false;
	
	@Override
	public void addElement(X x) {
		if(!built) {
			sequence.add(x);
		}
	}

	@Override
	public void removeElement(int position) {
		if(!built) {
			sequence.remove(position);
		}
		
	}

	@Override
	public void reverse() {
		if(!built) {
			Collections.reverse(sequence);
		}		
	}

	@Override
	public void clear() {
		if(!built) {
			sequence.clear();
		}
	}

	@Override
	public Optional<Sequence<X>> build() {
		if(!built) {
			built=true;
			return Optional.of(new Sequence<X>() {

				@Override
				public Optional<X> getAtPosition(int position) {
					if(position<sequence.size()) {
						return Optional.of(sequence.get(position));
					}
					return Optional.empty();
				}

				@Override
				public int size() {
					return sequence.size();
				}

				@Override
				public List<X> asList() {
					return sequence;
				}

				@Override
				public void executeOnAllElements(Executor<X> executor) {
					sequence.forEach(e->executor.execute(e));
				}
				
			});
		}
		return Optional.empty();
	}

	@Override
	public Optional<Sequence<X>> buildWithFilter(Filter<X> filter) {
		if(sequence.stream().filter(e->!filter.check(e)).findAny().isEmpty()) {	
			return build();
		}
		return Optional.empty();
	}

	@Override
	public <Y> SequenceBuilder<Y> mapToNewBuilder(Mapper<X, Y> mapper) {
		SequenceBuilder<Y> newSeq= new SequenceBuilderImpl<Y>();
		sequence.forEach(e->newSeq.addElement(mapper.transform(e)));
		return newSeq;
	}

}
