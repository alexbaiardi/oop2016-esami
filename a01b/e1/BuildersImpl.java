package a01b.e1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class BuildersImpl implements Builders {

	@Override
	public <X> ListBuilder<X> makeBasicBuilder() {
return new ListBuilder<X>() {
			
			List<X> list=new ArrayList<X>();
			boolean built=false;
			
			@Override
			public void addElement(X x) {
				check(!built);
				list.add(x);
			}

			@Override
			public List<X> build() {
				check(!built);
				built=true;
				return list;
			}
			
			private void check(boolean cond) {
				if(!cond) {
					throw new IllegalStateException();
				}
			}
		};
	}

	@Override
	public <X> ListBuilder<X> makeBuilderWithSize(int size) {
return new ListBuilder<X>() {
			
			List<X> list=new ArrayList<X>();
			boolean built=false;
			
			@Override
			public void addElement(X x) {
				check(!built);
				list.add(x);
			}

			@Override
			public List<X> build() {
				check(!built);
				check(size==list.size());
				built=true;
				return list;
			}
			
			private void check(boolean cond) {
				if(!cond) {
					throw new IllegalStateException();
				}
			}
		};
	}

	@Override
	public <X> ListBuilder<X> makeBuilderFromElements(Collection<X> from) {
			return new ListBuilder<X>() {
			
				List<X> list=new ArrayList<X>();
				boolean built=false;
				
				@Override
				public void addElement(X x) {
					check(!built);
					if(from.contains(x)) {
						list.add(x);
					} else {
						throw new IllegalArgumentException();
					}
					
					
				}
	
				@Override
				public List<X> build() {
					check(!built);
					built=true;
					return list;
				}
				
				private void check(boolean cond) {
					if(!cond) {
						throw new IllegalStateException();
					}
				}
			};
	}

	@Override
	public <X> ListBuilder<X> makeBuilderFromElementsAndWithSize(Collection<X> from, int size) {
		return new ListBuilder<X>() {
			
			List<X> list=new ArrayList<X>();
			boolean built=false;
			
			@Override
			public void addElement(X x) {
				check(!built);
				if(from.contains(x)) {
					list.add(x);
				} else {
					throw new IllegalArgumentException();
				}
				
				
			}

			@Override
			public List<X> build() {
				check(!built);
				check(size==list.size());
				built=true;
				return list;
			}
			
			private void check(boolean cond) {
				if(!cond) {
					throw new IllegalStateException();
				}
			}
		};		
	}

}
