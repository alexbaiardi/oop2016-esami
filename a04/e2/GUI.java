package a04.e2;

import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;


public class GUI {
    
    private JButton l = new JButton("l");
    private JButton r = new JButton("r");
    private JComboBox<String> box;
    private final Logic logic=new LogicImpl();
    
    public GUI(){
        box = new JComboBox<>();
        box.addItem(LogicImpl.SEQUENCE.get(0).toString());
        box.addItem(LogicImpl.SEQUENCE.get(1).toString());
        box.addItem(LogicImpl.SEQUENCE.get(2).toString());
        ActionListener al = e->{
        	logic.select(LogicImpl.SEQUENCE.get(((JComboBox)e.getSource()).getSelectedIndex()));
        };
        box.addActionListener(al);
        
        ActionListener ac= e -> {
        	JButton bt=(JButton)e.getSource();
        	logic.hit(bt.getText());
        	if(logic.isOver()) {
        		System.exit(0);
        	}
        };
        l.addActionListener(ac);
        r.addActionListener(ac);
        JPanel jp = new JPanel();
        jp.add(box);
        jp.add(l);
        jp.add(r);
        JFrame jf = new JFrame();
        jf.getContentPane().add(jp);
        jf.setSize(300, 100);
        jf.setVisible(true);
    }
    
    public static void main(String[] s){
        new GUI();
    }

}
