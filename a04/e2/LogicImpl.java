package a04.e2;

import java.util.List;

public class LogicImpl implements Logic {
	
	public static final List<List<String>> SEQUENCE=List.of(List.of("l","l","l","l","l"),List.of("r","r","r"),List.of("l","r","l","r"));
	
	private List<String> currentSequence=SEQUENCE.get(0);
	private int currentPos=0;

	@Override
	public void hit(String s) {
		this.currentPos=currentSequence.get(currentPos).equals(s)?currentPos+1:0;	
	}

	@Override
	public void select(List<String> sequence) {
		currentSequence=sequence;
	}

	@Override
	public boolean isOver() {
		return currentPos==currentSequence.size();
	}

}
